﻿using MailMarketing.Core.Entities;
using MailMarketing.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailMarketing.Core.Service.Default
{
    public class ContactService : ServiceBase<Contact>, IContactService
    {
        private readonly IContactRepository contactRepository;

        public ContactService(IContactRepository repository)
            :base(repository)
        {
            contactRepository = repository;
        }
    }
}
