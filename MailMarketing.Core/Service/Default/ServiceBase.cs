﻿using MailMarketing.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailMarketing.Core.Service.Default
{
    public class ServiceBase<TEntity> : IDisposable, IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> repositoryBase;

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            repositoryBase = repository;
        }

        public void Delete(TEntity t)
        {
            repositoryBase.Delete(t);
        }

        public void Dispose()
        {
            repositoryBase.Dispose();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return repositoryBase.GetAll();
        }

        public TEntity GetById(int id)
        {
            return repositoryBase.GetById(id);
        }

        public void Save(TEntity t)
        {
            repositoryBase.Save(t);
        }

        public void Update(TEntity t)
        {
            repositoryBase.Update(t);
        }
    }
}
