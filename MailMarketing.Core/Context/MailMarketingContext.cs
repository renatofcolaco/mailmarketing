﻿using MailMarketing.Core.Entities;
using MailMarketing.Core.EntityConfigurations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MailMarketing.Core.Context
{
    public class MailMarketingContext : DbContext
    {
        public DbSet<Contact> Contact { get; set; }

        public MailMarketingContext()
            :base("name=MailMarketingContext")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<MailMarketingContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new ContactConfiguration());
        }
    }
}
