﻿using MailMarketing.Core.Entities;
using System.Data.Entity.ModelConfiguration;

namespace MailMarketing.Core.EntityConfigurations
{
    public class ContactConfiguration : EntityTypeConfiguration<Contact>
    {
        public ContactConfiguration()
        {            
            Property(c => c.Name).IsRequired().HasMaxLength(50);
            Property(c => c.Email).IsRequired().HasMaxLength(50);
            Property(c => c.Active).IsRequired();
        }
    }
}
