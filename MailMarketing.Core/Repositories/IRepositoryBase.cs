﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailMarketing.Core.Repositories
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Save(TEntity t);
        void Delete(TEntity t);
        void Update(TEntity t);
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
        void Dispose();
    }
}
