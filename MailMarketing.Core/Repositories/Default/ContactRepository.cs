﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailMarketing.Core.Entities;

namespace MailMarketing.Core.Repositories.Default
{
    public class ContactRepository : RepositoryBase<Contact>, IContactRepository
    {
        
    }
}
