﻿using MailMarketing.Core.Context;
using MailMarketing.Core.Entities;
using MailMarketing.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailMarketing.Core.Repositories.Default
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected MailMarketingContext context = new MailMarketingContext();

        public void Delete(TEntity t)
        {
            context.Set<TEntity>().Remove(t);
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
            GC.SuppressFinalize(this);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return context.Set<TEntity>().ToList();
        }

        public TEntity GetById(int id)
        {
            return context.Set<TEntity>().Find(id);
        }

        public void Save(TEntity t)
        {
            context.Set<TEntity>().Add(t);
            context.SaveChanges();
        }

        public void Update(TEntity t)
        {
            context.Entry(t).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
