﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MailMarketing.Models
{
    public class ContactViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Por favor, informe o nome.")]
        [StringLength(50, ErrorMessage="O nome deve ser menor que 50 caracteres.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Por favor, informe o email.")]
        [StringLength(50, ErrorMessage = "O email deve ser menor que 50 caracteres.")]
        public string Email { get; set; }


        public bool Active { get; set; }
    }
}