﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MailMarketing.Core;
using MailMarketing.Core.Service.Default;
using MailMarketing.Core.Repositories.Default;
using AutoMapper;
using MailMarketing.Models;
using MailMarketing.Core.Entities;

namespace MailMarketing.Controllers
{
    public class ContactController : Controller
    {
        private readonly ContactService contactService = new ContactService(new ContactRepository());

        // GET: Contact
        public ActionResult Index()
        {
            var contacts = contactService.GetAll();
            var contactsViewModel = Mapper.Map<IList<ContactViewModel>>(contacts);
            return View(contactsViewModel);
        }

        // GET: Contact/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Contact/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contact/Create
        [HttpPost]
        public ActionResult Create(ContactViewModel newContact)//FormCollection collection
        {
            try
            {
                // TODO: Add insert logic here
                CreateOrUpdate(newContact);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(newContact);
            }
        }

        // GET: Contact/Edit/5
        public ActionResult Edit(int id)
        {
            var contact = contactService.GetById(id);
            var contactViewModel = Mapper.Map<ContactViewModel>(contact);

            return View("Create", contactViewModel);
        }

        // POST: Contact/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ContactViewModel newContact)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    CreateOrUpdate(newContact);
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("Create", newContact);
                }                
            }
            catch
            {
                return View("Create", newContact);
            }
        }

        // GET: Contact/Delete/5
        public ActionResult Delete(int id)
        {
            var contact = contactService.GetById(id);
            var contactViewModel = Mapper.Map<ContactViewModel>(contact);
            return View(contactViewModel);
        }

        // POST: Contact/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection formCollection)
        {
            try
            {
                // TODO: Add delete logic here
                var contact = contactService.GetById(id);
                contactService.Delete(contact);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private void CreateOrUpdate(ContactViewModel newContact)
        {
            if(newContact.Id == 0)
            {
                var contact = Mapper.Map<Contact>(newContact);
                contactService.Save(contact);
            }
            else
            {
                var contact = Mapper.Map<Contact>(newContact);
                contactService.Update(contact);
            }
        }
    }
}
