﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MailMarketing.Startup))]
namespace MailMarketing
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
