﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MailMarketing.Configuracao
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.AddProfile<EntitiesToViewModel>();
                cfg.AddProfile<ViewModelToEntities>();
            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}