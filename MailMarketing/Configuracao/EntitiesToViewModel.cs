﻿using AutoMapper;
using MailMarketing.Core.Entities;
using MailMarketing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MailMarketing.Configuracao
{
    public class EntitiesToViewModel : Profile
    {
        public EntitiesToViewModel()
        {
            CreateMap<Contact, ContactViewModel>();
        }
    }
}